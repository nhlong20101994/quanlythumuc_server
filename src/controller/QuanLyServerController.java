
package controller;

import java.awt.event.*;
import java.net.Socket;
import java.text.ParseException;
import java.util.Map;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import model.QuanLyServerModel;
import static model.QuanLyServerModel.ListClient;
import view.QuanLyServerView;

public class QuanLyServerController implements ActionListener, WindowListener, KeyListener {

    private QuanLyServerView QLSV_V;
    private QuanLyServerModel QLSV_MD;

    public QuanLyServerController(QuanLyServerView qlsv_v, QuanLyServerModel qlsv_md) {
        this.QLSV_V = qlsv_v;
        this.QLSV_MD = qlsv_md;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        switch (src) {
            case "-":
                QLSV_V.AnHien_DanhSachClient(true);
                return;
            case "+":
                QLSV_V.AnHien_DanhSachClient(false);
                return;
            case "Start":
                String port_S = QLSV_V.LayPort();
                int port_I = (KiemTraSo(port_S) ? Integer.parseInt(port_S) : -1);
                if (QLSV_MD.KhoiDong_Server(port_I, QLSV_V)) {
                    QLSV_V.AnHien_TrangThai(true);
                    QLSV_V.CapNhat_ThongBao_TrangThai(true);
                } else {
                    QLSV_V.Dialog_ThongBao("Khởi động Server không thành công!!!");
                }
                return;
            case "Stop":
                QLSV_MD.Dung_Server();
                QLSV_V.AnHien_TrangThai(false);
                QLSV_V.CapNhat_ThongBao_TrangThai(false);
                return;
            case "Tìm":
                String TuKhoa = QLSV_V.LayTuKhoa();
                if (!TuKhoa.equals("")) {
                    Map<Integer, Socket> KetQua = QLSV_MD.TimKiemClient(TuKhoa);
                    QLSV_V.XoaDanhSachClient();
                    for (Map.Entry<Integer, Socket> entry : KetQua.entrySet()) {
                        int port = entry.getKey();
                        Socket client = entry.getValue();
                        QLSV_V.ThemClientVaoDanhSach(client);
                    }

                }
                return;
            default:
                return;
        }
    }

    private boolean KiemTraSo(String so) {
        try {
            Integer.parseInt(so);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (QLSV_V.LayTuKhoa().equals("")) {
            Map<Integer, Socket> KetQua = QLSV_MD.LayDanhSach_Client();
            QLSV_V.XoaDanhSachClient();
            for (Map.Entry<Integer, Socket> entry : KetQua.entrySet()) {
                int port = entry.getKey();
                Socket client = entry.getValue();
                QLSV_V.ThemClientVaoDanhSach(client);
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
