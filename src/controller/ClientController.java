
package controller;

import java.awt.event.*;
import model.QuanLyServerModel;
import model.ThuMucModel;
import view.CayThuMucView;
import view.ClientView;
import view.QuanLyServerView;

public class ClientController implements ActionListener, MouseListener {
    private ClientView CL_V;
    private QuanLyServerView QLSV_V;
    private QuanLyServerModel QLSV_MD;

    public ClientController(ClientView cl_v, QuanLyServerView qlsv_v, QuanLyServerModel qlsv_md) {
        CL_V = cl_v;
        QLSV_V = qlsv_v;
        QLSV_MD = qlsv_md;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        switch (src) {
            case "-":
                return;
            default:
                return;
        }
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
        ThuMucModel ThuMuc = QLSV_MD.CayThuMuc_Client(CL_V.LayClient().getPort());
        CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLSV_V);
        QLSV_MD.CapNhat_Client_TheoDoi(CL_V.LayClient());
        QLSV_V.HienThiCayThuMuc(CayThuMuc);
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        
    }
}
