
package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class TapTinModel implements Serializable{
    private String TenDuongDan;
    private LocalDateTime ThoiGian_KhoiTao;
    private LocalDateTime LanCuoi_TruyCap;
    private LocalDateTime LanCuoi_ChinhSua;
    private String DinhDang;
    private String TenTapTin;
    private long KichThuoc;
    
    public TapTinModel(String tenduongdan, LocalDateTime thoigian_khoitao, LocalDateTime lancuoi_truycap, LocalDateTime lancuoi_chinhsua, String dinhdang, String tentaptin, long kichthuoc) {
        this.TenDuongDan = tenduongdan;
        this.ThoiGian_KhoiTao = thoigian_khoitao;
        this.LanCuoi_TruyCap = lancuoi_truycap;
        this.LanCuoi_ChinhSua = lancuoi_chinhsua;
        this.DinhDang = dinhdang;
        this.TenTapTin = tentaptin;
        this.KichThuoc = kichthuoc;
    }
    
    public TapTinModel() {
        this.TenDuongDan = null;
        this.ThoiGian_KhoiTao = null;
        this.LanCuoi_TruyCap = null;
        this.LanCuoi_ChinhSua = null;
        this.DinhDang = null;
        this.TenTapTin = null;
        this.KichThuoc = 0;
    }
    
    public LocalDateTime LayThoiGian_KhoiTao() {
        return ThoiGian_KhoiTao;
    }
    
    public LocalDateTime LayLanCuoi_TruyCap() {
        return LanCuoi_TruyCap;
    }
    
    public LocalDateTime LayLanCuoi_ChinhSua() {
        return LanCuoi_ChinhSua;
    }
    
    public String LayDinhDang() {
        return DinhDang;
    }
    
    public String LayTenTapTin() {
        return TenTapTin;
    }
    
    public long LayKichThuoc() {
        return KichThuoc;
    }
}
