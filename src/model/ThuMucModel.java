
package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

public class ThuMucModel implements Serializable{
    private String TenDuongDan;
    private LocalDateTime ThoiGian_KhoiTao;
    private LocalDateTime LanCuoi_TruyCap;
    private LocalDateTime LanCuoi_ChinhSua;
    private String TenThuMuc;
    private long KichThuoc;
    private List<TapTinModel> DanhSach_TapTin;
    private List<ThuMucModel> DanhSach_ThuMuc;
    
    public ThuMucModel(String tenduongdan, LocalDateTime thoigian_khoitao, LocalDateTime lancuoi_truycap, LocalDateTime lancuoi_chinhsua, String tenthumuc, long kichthuoc) {
        this.TenDuongDan = tenduongdan;
        this.ThoiGian_KhoiTao = thoigian_khoitao;
        this.LanCuoi_TruyCap = lancuoi_truycap;
        this.LanCuoi_ChinhSua = lancuoi_chinhsua;
        this.TenThuMuc = tenthumuc;
        this.KichThuoc = kichthuoc;
        this.DanhSach_TapTin = null;
        this.DanhSach_ThuMuc = null;
    }
    
    public ThuMucModel() {
        this.TenDuongDan = null;
        this.ThoiGian_KhoiTao = null;
        this.LanCuoi_TruyCap = null;
        this.LanCuoi_ChinhSua = null;
        this.TenThuMuc = null;
        this.KichThuoc = 0;
        this.DanhSach_TapTin = null;
        this.DanhSach_ThuMuc = null;
    }
    
    public void DanhSach_ThuMuc_TapTin(List<TapTinModel> danhsach_taptin, List<ThuMucModel> danhsach_thumuc) {
        DanhSach_TapTin = danhsach_taptin;
        DanhSach_ThuMuc = danhsach_thumuc;
    }
    
    public LocalDateTime LayThoiGian_KhoiTao() {
        return ThoiGian_KhoiTao;
    }
    
    public LocalDateTime LayLanCuoi_TruyCap() {
        return LanCuoi_TruyCap;
    }
    
    public LocalDateTime LayLanCuoi_ChinhSua() {
        return LanCuoi_ChinhSua;
    }
    
    public String LayTenThuMuc() {
        return TenThuMuc;
    }
    
    public long LayKichThuoc() {
        return KichThuoc;
    }
    
    public List<TapTinModel> LayDanhSach_TapTin() {
        return DanhSach_TapTin;
    }
    
    public List<ThuMucModel> LayDanhSach_ThuMuc() {
        return DanhSach_ThuMuc;
    }
}
