package model;

import java.io.*;
import java.net.*;
import java.util.Map;
import view.CayThuMucView;
import view.QuanLyServerView;

public class ThreadDoc extends Thread {

    private Socket Client;
    private QuanLyServerView QLSV_V;
    private QuanLyServerModel QLSV_MD;
    private ThuMucModel CayThuMuc_TheoDoi;

    public ThreadDoc(Socket client) {
        this.Client = client;
    }

    public void GetView(QuanLyServerView qlsv_v) {
        this.QLSV_V = qlsv_v;
    }

    public void GetModel(QuanLyServerModel qlsv_md) {
        this.QLSV_MD = qlsv_md;
    }

    @Override
    public void run() {
        ObjectInputStream ois = null;
        DuLieuTruyenQuaMangModel DuLieu_Nhan = null;
        try {
            while (true) {
                ois = new ObjectInputStream(Client.getInputStream());
                DuLieu_Nhan = (DuLieuTruyenQuaMangModel) ois.readObject();
                String LuaChon = DuLieu_Nhan.GetTieuDe();
                System.out.println("Lua chon: " + LuaChon);
                switch (LuaChon) {
                    case "Kết nối":
                        CayThuMuc_TheoDoi = DuLieu_Nhan.GetCayThuMuc();
                        QuanLyServerModel.ListClient.put(Client.getPort(), Client);
                        QuanLyServerModel.ListDaTaClient.put(Client.getPort(), CayThuMuc_TheoDoi);
                        QLSV_V.SetSoLuongClient(QuanLyServerModel.ListClient.size() + "");
                        QLSV_V.ThemClientVaoDanhSach(Client);
                        TapTinLogModel Log_KN = DuLieu_Nhan.GetTapTinLog();
                        Log_KN.SetIp(Client.getInetAddress().toString());
                        Log_KN.SetPort(Client.getPort());
                        QuanLyServerModel.DanhSachTapTinLog.add(Log_KN);
                        QLSV_MD.LuuTapTinLog(QuanLyServerModel.DanhSachTapTinLog);
                        QLSV_V.CapNhapLog(QLSV_MD.LayDanhSach_TapTinLog());
                        DuLieuTruyenQuaMangModel DuLieu_Truyen = new DuLieuTruyenQuaMangModel("Kết nối", null, "Xin kết nối.", null);
                        ThreadGhi TraLoi_KetNoi = new ThreadGhi(Client, DuLieu_Truyen);
                        TraLoi_KetNoi.start();
                        TraLoi_KetNoi.interrupt();
                        break;
                    case "Ngắt kết nối":
                        TapTinLogModel Log_NKN = DuLieu_Nhan.GetTapTinLog();
                        Log_NKN.SetIp(Client.getInetAddress().toString());
                        Log_NKN.SetPort(Client.getPort());
                        QuanLyServerModel.ListClient.remove(Client.getPort());
                        QuanLyServerModel.ListDaTaClient.remove(Client.getPort());
                        QLSV_V.SetSoLuongClient(QuanLyServerModel.ListClient.size() + "");
                        Map<Integer, Socket> KetQua = QLSV_MD.LayDanhSach_Client();
                        QLSV_V.XoaDanhSachClient();
                        for (Map.Entry<Integer, Socket> entry : KetQua.entrySet()) {
                            int port = entry.getKey();
                            Socket client = entry.getValue();
                            QLSV_V.ThemClientVaoDanhSach(client);
                        }
                        QuanLyServerModel.DanhSachTapTinLog.add(Log_NKN);
                        QLSV_V.CapNhapLog(QLSV_MD.LayDanhSach_TapTinLog());
                        break;
                    case "Cập nhật thư mục":
                        ThuMucModel ThuMuc_CapNhat = DuLieu_Nhan.GetCayThuMuc();
                        if (QuanLyServerModel.ListDaTaClient.containsKey(Client.getPort())) {
                            QuanLyServerModel.ListDaTaClient.replace(Client.getPort(), ThuMuc_CapNhat);
                            if (QLSV_MD.KiemTra_Client_TheoDoi(Client)) {
                                ThuMucModel ThuMuc = QLSV_MD.CayThuMuc_Client(Client.getPort());
                                CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLSV_V);
                                TapTinLogModel Log = DuLieu_Nhan.GetTapTinLog();
                                Log.SetIp(Client.getInetAddress().toString());
                                Log.SetPort(Client.getPort());
                                QLSV_V.HienThiCayThuMuc(CayThuMuc);
                                QuanLyServerModel.DanhSachTapTinLog.add(Log);
                                QLSV_MD.LuuTapTinLog(QuanLyServerModel.DanhSachTapTinLog);
                                QLSV_V.CapNhapLog(QLSV_MD.LayDanhSach_TapTinLog());
                            }
                        }
                        break;
                    case "Thêm":
                        ThuMucModel ThuMuc_Them = DuLieu_Nhan.GetCayThuMuc();
                        if (QuanLyServerModel.ListDaTaClient.containsKey(Client.getPort())) {
                            QuanLyServerModel.ListDaTaClient.replace(Client.getPort(), ThuMuc_Them);
                            if (QLSV_MD.KiemTra_Client_TheoDoi(Client)) {
                                ThuMucModel ThuMuc = QLSV_MD.CayThuMuc_Client(Client.getPort());
                                CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLSV_V);
                                TapTinLogModel Log = DuLieu_Nhan.GetTapTinLog();
                                Log.SetIp(Client.getInetAddress().toString());
                                Log.SetPort(Client.getPort());
                                QLSV_V.HienThiCayThuMuc(CayThuMuc);
                                QuanLyServerModel.DanhSachTapTinLog.add(Log);
                                QLSV_MD.LuuTapTinLog(QuanLyServerModel.DanhSachTapTinLog);
                                QLSV_V.CapNhapLog(QLSV_MD.LayDanhSach_TapTinLog());
                            }
                        }
                        break;
                    case "Sửa nội dung":
                        ThuMucModel ThuMuc_NoiDung = DuLieu_Nhan.GetCayThuMuc();
                        if (QuanLyServerModel.ListDaTaClient.containsKey(Client.getPort())) {
                            QuanLyServerModel.ListDaTaClient.replace(Client.getPort(), ThuMuc_NoiDung);
                            if (QLSV_MD.KiemTra_Client_TheoDoi(Client)) {
                                ThuMucModel ThuMuc = QLSV_MD.CayThuMuc_Client(Client.getPort());
                                CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLSV_V);
                                TapTinLogModel Log = DuLieu_Nhan.GetTapTinLog();
                                Log.SetIp(Client.getInetAddress().toString());
                                Log.SetPort(Client.getPort());
                                QLSV_V.HienThiCayThuMuc(CayThuMuc);
                                QuanLyServerModel.DanhSachTapTinLog.add(Log);
                                QLSV_MD.LuuTapTinLog(QuanLyServerModel.DanhSachTapTinLog);
                                QLSV_V.CapNhapLog(QLSV_MD.LayDanhSach_TapTinLog());
                            }
                        }
                        break;
                    case "Xóa":
                        ThuMucModel ThuMuc_Xoa = DuLieu_Nhan.GetCayThuMuc();
                        if (QuanLyServerModel.ListDaTaClient.containsKey(Client.getPort())) {
                            QuanLyServerModel.ListDaTaClient.replace(Client.getPort(), ThuMuc_Xoa);
                            if (QLSV_MD.KiemTra_Client_TheoDoi(Client)) {
                                ThuMucModel ThuMuc = QLSV_MD.CayThuMuc_Client(Client.getPort());
                                CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLSV_V);
                                TapTinLogModel Log = DuLieu_Nhan.GetTapTinLog();
                                Log.SetIp(Client.getInetAddress().toString());
                                Log.SetPort(Client.getPort());
                                QLSV_V.HienThiCayThuMuc(CayThuMuc);
                                QuanLyServerModel.DanhSachTapTinLog.add(Log);
                                QLSV_MD.LuuTapTinLog(QuanLyServerModel.DanhSachTapTinLog);
                                QLSV_V.CapNhapLog(QLSV_MD.LayDanhSach_TapTinLog());
                            }
                        }
                        break;
                    case "Đổi tên":
                        ThuMucModel ThuMuc_DoiTen = DuLieu_Nhan.GetCayThuMuc();
                        if (QuanLyServerModel.ListDaTaClient.containsKey(Client.getPort())) {
                            QuanLyServerModel.ListDaTaClient.replace(Client.getPort(), ThuMuc_DoiTen);
                            if (QLSV_MD.KiemTra_Client_TheoDoi(Client)) {
                                ThuMucModel ThuMuc = QLSV_MD.CayThuMuc_Client(Client.getPort());
                                CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLSV_V);
                                TapTinLogModel Log = DuLieu_Nhan.GetTapTinLog();
                                Log.SetIp(Client.getInetAddress().toString());
                                Log.SetPort(Client.getPort());
                                QLSV_V.HienThiCayThuMuc(CayThuMuc);
                                QuanLyServerModel.DanhSachTapTinLog.add(Log);
                                QLSV_MD.LuuTapTinLog(QuanLyServerModel.DanhSachTapTinLog);
                                QLSV_V.CapNhapLog(QLSV_MD.LayDanhSach_TapTinLog());
                            }
                        }
                        break;
                    default:
                        break;
                }

            }
        } catch (IOException ex) {
            try {
                ois.close();
            } catch (IOException e) {
                System.out.println("error: " + e.getMessage());
            }
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            System.out.println("error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
