
package model;

import java.io.*;
import java.net.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import view.QuanLyServerView;


public class QuanLyServerModel {
    private String TenThuMuc_GhiLog_MacDinh = "C:/ServerMonitoringSystem/Log";
    private String TenTapTin_GhiLog_MacDinh = "C:/ServerMonitoringSystem/Log/TapTinLog.txt";
    public static Map<Integer, Socket> ListClient = new HashMap<Integer, Socket>();
    public static Map<Integer, ThuMucModel> ListDaTaClient = new HashMap<Integer, ThuMucModel>();
    public static List<TapTinLogModel> DanhSachTapTinLog = new ArrayList<>();
    private ServerSocket Server;
    private Socket ClientTheoDoi;
    
    public QuanLyServerModel() {
        TaoThuMuc(TenThuMuc_GhiLog_MacDinh);
        TaoTapTin(TenTapTin_GhiLog_MacDinh);
        DanhSachTapTinLog = DocTapTinLog();
    }
    
    public String[][] LayDanhSach_TapTinLog() {
        String[][] DSTL = null;
        if (DanhSachTapTinLog.size() <= 0) {
            return new String[][]{};
        }
        
        DSTL = new String[DanhSachTapTinLog.size()][6];
        int i = 0;
        for (TapTinLogModel log : DanhSachTapTinLog) {
            DSTL[i] = new String[] {"<html><center>"+log.GetSTT(),
                                    "<html><center>"+DinhDang_ThoiGian(log.GetThoiDiem()),
                                    "<html><center>"+log.GetHanhDong(),
                                    "<html><center>"+log.GetIp(),
                                    "<html><center>"+log.GetPort()+"",
                                    "<html><center>"+log.GetDienGiaiHanhDong()};
            i++;
        }
        
        return DSTL;
    }
    
    public boolean KhoiDong_Server(int port, QuanLyServerView qlsv_v) {
        System.out.println("ClientTheoDoi khởi tạo: " + ClientTheoDoi);
        try {
            Server = new ServerSocket(port);
            
            ThreadLangNgheKetNoi LangNghe_KetNoi = new ThreadLangNgheKetNoi(Server);
            LangNghe_KetNoi.start();
            LangNghe_KetNoi.GetView(qlsv_v);
            LangNghe_KetNoi.GetModel(this);
            System.out.println("Server lắng nghe từ port " + Server.getLocalPort());
        } catch (IOException ex) {
            return false;
        } catch (IllegalArgumentException ex) {
            return false;
        }
        return true;
    }
    
    public boolean KiemTra_Client_TheoDoi(Socket client) {
        if(ClientTheoDoi == null) return false;
        if(ClientTheoDoi.equals(client)){
            return true;
        }
        return false;
    }
    
    public void CapNhat_Client_TheoDoi(Socket client) {
        ClientTheoDoi = client;
        System.out.println("ClientTheoDoi cập nhật " + ClientTheoDoi);
    }
    
    public void Dung_Server() {
        try {
            if(Server != null) {
                Server.close();
                System.out.println("Server disconnect!");
            }
        } catch (IOException ex) {
            System.out.println("error: " + ex.getMessage());
        }
    }
    
    public ThuMucModel CayThuMuc_Client(int port) {
        ThuMucModel ThuMuc = null;
        if(ListDaTaClient.containsKey(port)) {
            return ListDaTaClient.get(port);
        }
        return ThuMuc;
    }
    
    public Map<Integer, Socket> LayDanhSach_Client() {
        return ListClient;
    }
    
    public Map<Integer, Socket> TimKiemClient(String tukhoa) {
        Map<Integer, Socket> KetQua = new HashMap<Integer, Socket>();
        for(Map.Entry<Integer, Socket> entry : ListClient.entrySet()) {
            int port =  entry.getKey();
            Socket client = entry.getValue();
            String DuLieu = client.getInetAddress() + " - " + port;
            if(DuLieu.contains(tukhoa)) {
                KetQua.put(port, client);
            }
        }
        return KetQua;
    }
    
    public ServerSocket LayServer() {
        return Server;
    }
    
    private void TaoTapTin(String file_name) {
        File file = new File(file_name);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Tap tin loi" + e);
            }
        }
    }
    
    private void TaoThuMuc(String thumuc) {
        if (new File(thumuc).isDirectory()) {
            return;
        }
        String ThuMuc_XuLy = "";
        String[] DanhSachTen_ThuMuc = thumuc.split("/");
        for (String s : DanhSachTen_ThuMuc) {
            ThuMuc_XuLy = ThuMuc_XuLy + s + "/";
            File file = new File(ThuMuc_XuLy);
            if (!file.isDirectory()) {
                file.mkdir();
            }
        }
    }
    
    public void LuuTapTinLog(List<TapTinLogModel> taptinlog) {
        GhiTapTinNhiPhan(taptinlog, TenTapTin_GhiLog_MacDinh);
    }
    
    public List<TapTinLogModel> DocTapTinLog() {
        return DocTapTinNhiPhan(TenTapTin_GhiLog_MacDinh);
    }
    
    private void GhiTapTinNhiPhan(List<TapTinLogModel> taptinlog, String diachi_taptin){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(diachi_taptin);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(taptinlog);
        } catch (FileNotFoundException e) {
            System.out.println("error: " + e.getMessage());
        } catch(Exception e){
            e.printStackTrace();
        }
        finally{
            closeOS(oos);
            closeOS(fos);
        }
    }
    
    private List<TapTinLogModel> DocTapTinNhiPhan(String diachi_taptin) {
        List<TapTinLogModel> TapTinLog = new ArrayList<>();
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        if(KiemTraTapTin(diachi_taptin) == false) {
            return TapTinLog;
        }
        try {
            fis = new FileInputStream(diachi_taptin);
            ois = new ObjectInputStream(fis);
            TapTinLog = (List<TapTinLogModel>) ois.readObject();
        } catch (Exception e) {
            return TapTinLog;
        }
        finally{
            closeIS(ois);
            closeIS(fis);
        }
        return TapTinLog;
    }

    public String DinhDang_ThoiGian(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }
    
    private boolean KiemTraTapTin(String file_name) {
        File file = new File(file_name);
        if (!file.exists()) {
            return false;
        }
        return true;
    }
    
    private void closeOS(OutputStream os){
        try {
            if(os != null){
                os.close();
            }
        } catch (Exception e) {
            System.out.println("Error Close file output: " + e);
        }
    }
    
    private void closeIS(InputStream is){
        try {
            if(is != null){
                is.close();
            }
        } catch (Exception e) {
            System.out.println("Error Close file input: " + e);
        }
    }
}
