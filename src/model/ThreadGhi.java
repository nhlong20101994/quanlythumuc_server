
package model;

import java.io.*;
import java.net.*;

/**
 *
 * @author ASUS
 */
public class ThreadGhi extends Thread {
    private Socket Client;
    private DuLieuTruyenQuaMangModel DuLieuGui;

    public ThreadGhi(Socket client, DuLieuTruyenQuaMangModel dulieugui) {
        this.Client = client;
        this.DuLieuGui = dulieugui;
    }

    @Override
    public void run() {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(Client.getOutputStream());
            oos.writeObject(DuLieuGui);
            System.out.println("Đã gửi thành công đến Client");
        } catch (Exception e) {
            try {
                oos.close();
            } catch (Exception ex) {
                System.out.println("Ngắt kết nối Client.");
            }
        }
    }
}
