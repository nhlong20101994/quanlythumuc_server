
package model;

import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import view.QuanLyServerView;

public class ThreadLangNgheKetNoi extends Thread {
    private Thread t;
    private ServerSocket Server;
    private Socket Client;
    private QuanLyServerView QLSV_V;
    private QuanLyServerModel QLSV_MD;
    
    public ThreadLangNgheKetNoi(ServerSocket server) {
        Server = server;
    }
    
    public void GetView(QuanLyServerView qlsv_v) {
        this.QLSV_V = qlsv_v;
    }
    
    public void GetModel(QuanLyServerModel qlsv_md) {
        this.QLSV_MD = qlsv_md;
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                Client = Server.accept();
                System.out.println("ip-port: " + Client.getInetAddress() + " - " + Client.getLocalPort());
                ThreadDoc Doc = new ThreadDoc(Client);
                Doc.start();
                Doc.GetView(QLSV_V);
                Doc.GetModel(QLSV_MD);
            } catch (IOException ex) {
                System.out.println("error: " + ex.getMessage());
            }
        }
    }
    
    public void start() {
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }
}
