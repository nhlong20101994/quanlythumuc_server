
package view;

import controller.ClientController;
import controller.QuanLyServerController;
import java.awt.*;
import java.net.Socket;
import javax.swing.*;
import model.QuanLyServerModel;

public class ClientView extends JPanel implements ICommon {
    private int Rong = 290;
    private int Cao = 50;
    private String Ten;
    private String Ip;
    private int Port;
    private Socket Client;
    private boolean TinhTrang_TheoDoi;
    private QuanLyServerModel QLSV_MD;
    private QuanLyServerView QLSV_V;
    private QuanLyServerController QLSV_CTL;
    
    private SpringLayout Layout;
    
    private JLabel jLabel_HinhAnh;
    private JLabel jLabel_Ten;
    
    
    public ClientView(QuanLyServerModel qlsv_md, QuanLyServerView qlsv_v, QuanLyServerController qlsv_ctl, String ten, Socket client, boolean tinhtrangtheodoi) {
        Ten = ten;
        Client = client;
        Ip = client.getInetAddress().toString();
        Ten = Ip;
        Port = client.getPort();
        TinhTrang_TheoDoi = tinhtrangtheodoi;
        
        QLSV_MD = qlsv_md;
        QLSV_V = qlsv_v;
        QLSV_CTL = qlsv_ctl;
        initComponent();
        addComponent();
        addEvent();
    }

    @Override
    public void initComponent() {
        setSize(Rong, Cao);
        setPreferredSize(new Dimension(Rong, Cao));
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBackground(Color.WHITE);
        setLayout(new BorderLayout());
    }

    @Override
    public void addComponent() {
        Layout = new SpringLayout();
        
        jLabel_HinhAnh = new JLabel(LayChuCaiDau_Ten());
        jLabel_HinhAnh.setSize(new Dimension(Cao, Cao));
        jLabel_HinhAnh.setPreferredSize(new Dimension(Cao, Cao));
        jLabel_HinhAnh.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        jLabel_HinhAnh.setHorizontalAlignment(SwingConstants.CENTER);
        jLabel_HinhAnh.setVerticalAlignment(SwingConstants.CENTER);
        
        jLabel_Ten = new JLabel("IP: " + Ip + " - PORT: " + Port);
        jLabel_Ten.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        jLabel_Ten.setHorizontalAlignment(SwingConstants.CENTER);
        jLabel_Ten.setVerticalAlignment(SwingConstants.CENTER);

        add(jLabel_HinhAnh, BorderLayout.WEST);
        add(jLabel_Ten, BorderLayout.CENTER);
    }

    @Override
    public void addEvent() {
        ClientController cl_v = new ClientController(this, QLSV_V, QLSV_MD);
        this.addMouseListener(cl_v);
    }
    
    public int LayPort() {
        return Port;
    }
    
    public Socket LayClient() {
        return Client;
    }
    
    private String LayChuCaiDau_Ten() {
        return Ten.substring(0, 1).toUpperCase();
    }
}
